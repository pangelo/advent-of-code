# [Advent of Code Puzzle #6](http://adventofcode.com/2015/day/6)

import sys

WIRES = {}

OPCODES = {
    'AND': lambda i: i[0].value & i[1].value,
    'OR': lambda i: i[0].value | i[1].value,
    'NOT': lambda i: 65536 + ~(i[0].value),
    'LSHIFT': lambda i: i[0].value << i[1].value,
    'RSHIFT': lambda i: i[0].value >> i[1].value
}

class Wire:
    def __init__(self, value = -1):
        self.value = -1
        self.ready = False
        self.put(value)

    def put(self, value):
        self.value = value
        if self.value < 0:
            self.ready = False
        else:
            self.ready = True

    def get(self):
        return self.value

def fv(v):
    "fetch a value either from a wire or a raw integer"
    if v.isnumeric():
        return int(v)
    else:
        return WIRES[v]

def check_wire(w):
    if w.isnumeric():
        return Wire(int(w))
    if w in WIRES:
        return WIRES[w]
    else:
        WIRES[w] = Wire()
        return WIRES[w]

# 123 -> x        : ('ASSIGN', 123, 'x')
# x AND y -> d    : ('AND', 'x', 'y', 'd')
# x OR y -> e     : ('OR', 'x', 'y', 'e')
# x LSHIFT 2 -> f : ('LSHIFT', 'x', 2, 'f')
# y RSHIFT 2 -> g : ('RSHIFT', 'y', 2, 'g')
# NOT x -> h      : ('NOT', 'x', 'h')
def parse_instructions(s):
    g_in, g_out = [x.strip() for x in s.split("->")]

    g_op = 'ASSIGN'
    for op in OPCODES.keys():
        if op in g_in:
            g_op = op
            g_in1, g_in2 = [x.strip() for x in g_in.split(op)]
            break

    result = [g_op]
    if g_op == 'ASSIGN':
        result.append(check_wire(g_in))
    elif g_op == 'NOT':
        result.append(check_wire(g_in2))
    elif g_op == 'AND' or g_op == 'OR':
        result.append(check_wire(g_in1))
        result.append(check_wire(g_in2))
    elif g_op == 'LSHIFT' or g_op == 'RSHIFT':
        result.append(check_wire(g_in1))
        result.append(check_wire(g_in2))
    else:
        result = ['UNKNOWN']
    result.append(check_wire(g_out))
    return tuple(result)

def check_deferred(op):
    op_id = op[0]
    if op_id == 'AND' or op_id == 'OR':
        return (not op[1].ready or not op[2].ready)
    else:
        # ASSIGN, NOT, LSHIFT, RSHIFT
        return not op[1].ready


def main():
    ops = []
    for line in sys.stdin:
        ops.append(parse_instructions(line))

    while ops:
        deferred_ops = []
        for op in ops:
            op_id = op[0]
            op_out = op[-1]
            if check_deferred(op):
                deferred_ops.append(op)
                continue
            if op_id == 'ASSIGN':
                op_out.put(op[1].value)
            else:
                op_out.put(OPCODES[op_id](op[1:3]))
        ops = deferred_ops
    
    for key in sorted(WIRES.keys()):
        print ("{}: {}".format(key, WIRES[key].value))


if __name__ == '__main__':
    main()
