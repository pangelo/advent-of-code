# [Advent of Code 2015 Puzzle #1](http://adventofcode.com//2015/day/1)

import sys

floor = 0
op_count = 0
basement_op = 0

symbols = {
    '(': 1,
    ')': -1
}

for op in sys.stdin.read():
    op_count = op_count + 1
    floor = floor + symbols.get(op,0)
    if floor == -1 and basement_op == 0:
        basement_op = op_count

print("end up on floor {}, first enter basement on pos {}".format(floor, basement_op))

