# [Advent of Code Puzzle #2](http://adventofcode.com/2015/day/2)

import sys

# total area of paper to order
total_paper = 0
total_ribbon = 0

def surface_area(dims):
    l,w,h = dims
    return 2*l*w + 2*w*h + 2*h*l

def min_area(dims):
    l,w,h = dims
    areas = (l*w, w*h, h*l)
    return min(areas)

def volume(dims):
    l,w,h = dims
    return l*w*h

def min_perimeter(dims):
    l,w,h = dims
    perims = (2*(l+w), 2*(w+h), 2*(h+l))
    return min(perims)
    
for line in sys.stdin:
    # length, width, height
    box_dims = tuple(int(x) for x in line.split("x"))
    box_paper = surface_area(box_dims) + min_area(box_dims)
    box_ribbon = min_perimeter(box_dims) + volume(box_dims)
    total_paper = total_paper + box_paper
    total_ribbon = total_ribbon + box_ribbon

print ("total amount of paper to order: {} square feet".format(total_paper))
print ("total amount of ribbon to order: {} feet".format(total_ribbon))
