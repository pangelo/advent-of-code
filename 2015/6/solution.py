# [Advent of Code Puzzle #6](http://adventofcode.com/2015/day/6)

import sys
import re

SIZE = 1000

opcodes_1 = {
    'turn on': lambda x: 1,
    'turn off': lambda x: 0,
    'toggle': lambda x: 1 - x
}

opcodes_2 = {
    'turn on': lambda x: x + 1,
    'turn off': lambda x: max(0, x - 1),
    'toggle': lambda x: x + 2
}

INSTRUCTION_RE = re.compile(r'(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)')

def parse_instructions(s):
    captures = INSTRUCTION_RE.match(s).groups()
    return (captures[0], int(captures[1]), int(captures[2]), int(captures[3]), int(captures[4]))

def fill(surface, x0, y0, x1, y1, op):
    for i in range(x0, x1 + 1):
        for j in range(y0, y1 + 1):
            surface[i][j] = op(surface[i][j])

def sum_values(surface):
    result = 0
    for line in surface:
        for value in line:
            result = result + value
    return result

def main(surface, size, opcodes):
    fill (surface, 0, 0, size - 1, size - 1, opcodes['turn off'])
    for line in sys.stdin:
        cmd,x0,y0,x1,y1 = parse_instructions(line.strip())
        op = opcodes.get (cmd, lambda x: x)
        fill(surface, x0, y0, x1, y1, op)

    result = sum_values(surface)
    print ("result is {}".format(result))
        
if __name__ == '__main__':
    size = 1000
    surface = [[0] * size for i in range(size)]

    #main(surface, size, opcodes_1)
    main(surface, size, opcodes_2)
