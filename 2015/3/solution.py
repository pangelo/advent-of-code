# [Advent of Code Puzzle #3](http://adventofcode.com/2015/day/3)

import sys

bot_idx = 0
bot_coords = [[0,0],[0,0]]

ops = {
    ">": (1,0),
    "<": (-1,0),
    "^": (0,1),
    "v": (0,-1)
}

deliveries = {
    (0,0): 1
}

def update_coords(coords, delta):
    coords[0] = coords[0] + delta[0]
    coords[1] = coords[1] + delta[1]

for op in sys.stdin.read():
    delta = ops.get(op, (0,0))
    update_coords(bot_coords[bot_idx], delta)
    key = tuple(bot_coords[bot_idx])
    deliveries[key] = 1
    bot_idx = abs(bot_idx - 1)
    
print ("delivered presents to {} houses".format(len(deliveries)))
