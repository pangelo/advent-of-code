# [Advent of Code Puzzle #4](http://adventofcode.com/2015/day/4)

import sys
import hashlib

TARGET_ZEROS = 6

def count_zeros(hash):
    nzeros = 0 
    for c in hash:
        if c == '0':
            nzeros = nzeros + 1
        else:
            break
    return nzeros

def main():
    key = sys.stdin.readline().strip()
    nonce = 1

    print("working...")
    
    while True:
        hasher = hashlib.md5()
        source = key + str(nonce)
        hasher.update(bytes(source))
        nzeros = count_zeros(hasher.hexdigest())
        if nzeros >= TARGET_ZEROS:
            print ("result is {} ({})".format(nonce, hasher.hexdigest()))
            break
        nonce = nonce + 1

if __name__ == "__main__":
    main()
