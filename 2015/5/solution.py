# [Advent of Code Puzzle #5](http://adventofcode.com/2015/day/5)

import sys

#
# rule functions
#

def num_vowels(s):
    nvowels = 0
    for c in s:
        if c in ('a','e','i','o','u'):
            nvowels = nvowels + 1
    return nvowels

def has_double_letter_seq(s):
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            return True
    return False

def has_forbidden_seq(s):
    for fseq in ('ab','cd','pq','xy'):
        if fseq in s:
            return True
    return False

def has_double_pair(s):
    for pi in range(len(s) - 2):
        for i in range(pi + 2, len(s) - 1):
            if s[pi] == s[i] and s[pi + 1] == s[i + 1]:
                return True
    return False

def has_mirror_pair(s):
    for i in range(len(s) - 2):
        if s[i] == s[i+2]:
            return True
    return False

#
# rule sets for both versions of the problem
#

def check_nice_1(s):
    return num_vowels(s) >= 3 and has_double_letter_seq(s) and not has_forbidden_seq(s)

def check_nice_2(s):
    return has_double_pair(s) and has_mirror_pair(s)

# active rule set
CHECK_NICE = check_nice_2

def main():
    num_nice = 0
    for line in sys.stdin:
        if CHECK_NICE(line.strip()):
            num_nice = num_nice + 1
    print ("there are {} nice strings in the input".format(num_nice))
            
if __name__ == '__main__':
    main()
